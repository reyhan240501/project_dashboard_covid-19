<?php

$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, 'https://api.covid19api.com/summary');
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($curl);
curl_close($curl);

$result = json_decode($result, true);

$global = $result['Global'];
$temp_NewConfirmed = (string)$global['NewConfirmed'];
$TotalConfirmed = (string) $global['TotalConfirmed'];
$temp_NewDeaths = (string)$global['NewDeaths'];
$TotalDeaths = (string) $global['TotalDeaths'];
$temp_NewRecovered = (string)$global['NewRecovered'];
$TotalRecovered = (string) $global['TotalRecovered'];
$countries = $result['Countries'];


?>   

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"
    integrity="sha512-d9xgZrVZpmmQlfonhQUvTR7lMPtO7NkZMkA0ABN3PHCbKA5nqylQ/yWlFAyY6hYgdF1Qh6nYiuADWwKB4C2WSw=="
    crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    
    <style>
        .country {
            overflow: auto;
            height: 447px;
        }
    </style>
    
    <title>Dashboard Covid-19</title>
</head>
<body class="bg-secondary">
    <div id="header" class="text-light text-center p-1 bg-dark">
        <h2 class="font-weight-light">COVID-19 RESOURCE CENTER</h2>
        <h6 class="font-weight-light">Update Globally, as of <?= $countries[0]['Date'];?> </h6>
    </div>
    
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-3 col-lg-3 mt-2">
                <div class="card-header text-light text-center p-1 shadow-sm bg-white rounded" style="background-image: linear-gradient(to bottom, #708090, black);">
                    <h5>Country</h5>
                </div>
                <div class="country shadow-sm bg-white rounded">
                    <?php foreach($countries as $key=>$value):?>
                    <ul  class="navbar-nav mr-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-light bg-dark" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <?= $value['Country'];?>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <p class="dropdown-item">Total Confirmed: <?= $value['TotalConfirmed'];?> </p>
                                <p class="dropdown-item">Total Deaths: <?= $value['TotalDeaths'];?> </p>
                                <p class="dropdown-item" >Total Recovered: <?= $value['TotalRecovered'];?> </p>
                            </div>
                        </li>
                    </ul>
                    <?php endforeach;?>
                </div>
            </div>
            
            <div class="col-sm-12 col-md-9 col-lg-9 shadow-sm bg-white rounded p-2 mt-2" style="background-image: linear-gradient(to bottom, #708090, black);">
                <div class="col-sm-12 col-md-4 col-lg-4 float-left">
                    <div class="card text-center">
                        <div class="card-header" style="background-color: #FA8072;">
                            <h3 class="text-light">Confirmed</h3>
                            <h4 class="text-light mb-0">(Global)</h4>
                        </div>
                        <div class="card-body p-0">
                            <h4 class="card-text"> <?= number_format($temp_NewConfirmed); ?></h4>
                            <p>(New Confirmed)</p>
                            <h4 class="card-title"> <?= number_format($TotalConfirmed);?> </h4>
                            <p>(Total Confirmed)</p>
                        </div>
                    </div>
                    <h4 class="mt-4 text-center text-light">Confirmed</h4>
                    <canvas id="confirmed" class="mt-2"></canvas>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4 float-left">
                    <div class="card text-center">
                        <div class="card-header" style="background-color: #2C3E50;">
                            <h3 class="text-light">Deaths</h3>
                            <h4 class="text-light mb-0">(Global)</h4>
                        </div>
                        <div class="card-body p-0"> 
                            <h4 class="card-text"> <?= number_format($temp_NewDeaths); ?></h4>
                            <p>(New Deaths)</p>
                            <h4 class="card-title"> <?= number_format($TotalDeaths);?> </h4>
                            <p>(Total Deaths)</p>
                        </div>
                    </div>
                    <h4 class="mt-4 text-center text-light">Deaths</h4>
                    <canvas id="deaths" class="mt-2"></canvas>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4 float-left">
                    <div class="card text-center">
                        <div class="card-header" style="background-color: #11875D;">
                            <h3 class="text-light">Recovered</h3>
                            <h4 class="text-light mb-0">(Global)</h4>
                        </div>
                        <div class="card-body p-0">
                            <h4 class="card-text"> <?= number_format($temp_NewRecovered); ?></h4>
                            <p>(New Recovered)</p>
                            <h4 class="card-title"> <?= number_format($TotalRecovered);?> </h4>
                            <p>(Total Recovered)</p>
                        </div>
                    </div>
                    <h4 class="mt-4 text-center text-light">Recovered</h4>
                    <canvas id="recovered" class="mt-2"></canvas>
                </div>
            </div>
        </div>    
    </div>  
    
    <script>
        var confirmed = document.getElementById('confirmed').getContext('2d');
        var death = document.getElementById('deaths').getContext('2d');
        var recovery = document.getElementById('recovered').getContext('2d');
        
        var data = $.ajax({
            url: "https://api.covid19api.com/summary",
            cache: false
        })
        .done(function (covid) {
            
            function getCountries(covid) {
                var temp_country=[];
                
                covid.Countries.forEach(function(el){
                    temp_country.push(el.Country);
                })
                return temp_country;
            }
            
            function getConfirmed(covid) {
                var temp_confirmed=[];
                
                covid.Countries.forEach(function(el) {
                    temp_confirmed.push(el.TotalConfirmed)
                })
                return temp_confirmed;
            }
            
            function getDeaths(covid) {
                var temp_deaths = [];
                
                covid.Countries.forEach(function(el) {
                    temp_deaths.push(el.TotalDeaths)
                })
                return temp_deaths;
            }
            
            function getRecovered(covid) {
                var temp_recovered = [];
                
                covid.Countries.forEach(function(el) {
                    temp_recovered.push(el.TotalRecovered)
                })
                return temp_recovered;
            }
            
            var colors = [];
            function getRandomColor() {
                var r = Math.floor(Math.random() * 255);
                var g = Math.floor(Math.random() * 255);
                var b = Math.floor(Math.random() * 255);
                return "rgb(" + r + "," + g + "," + b + ")";
            }     
            
            for (var i in covid.Countries) {
                colors.push(getRandomColor());
            }
            
            var confirmedPieChart = new Chart(confirmed,{
                type: 'pie',
                data: {
                    labels: getCountries(covid),
                    datasets: [{
                        backgroundColor: colors,
                        label: '# Total',
                        data: getConfirmed(covid),
                        borderWidth: 0.5
                    }]
                }, 
                options: {
                    legend: {
                        responsive: true,
                        display: false
                    }
                }
            })
            
            var deathsPieChart = new Chart(deaths,{
                type: 'pie',
                data: {
                    labels: getCountries(covid),
                    datasets: [{
                        backgroundColor: colors,
                        label: '# Total',
                        data: getDeaths(covid),
                        borderWidth: 0.5
                    }]
                }, 
                options: {
                    legend: {
                        display: false,
                    }
                }
            })
            
            var recoveredPieChart = new Chart(recovered,{
                type: 'pie',
                data: {
                    labels: getCountries(covid),
                    datasets: [{
                        backgroundColor: colors,
                        label: '# Total',
                        data: getRecovered(covid),
                        borderWidth: 0.5
                    }]
                }, 
                options: {
                    legend: {
                        display: false,
                    }
                }
            })
        });
    </script>
    
    
    
    <!-- Optional JavaScript; choose one of the two! -->
    
    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    
    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    -->
</body>
</html>